$(document).ready(function(){
	$('.bxslider').bxSlider({
		pager: false,
		startSlide: 0,
		mode: 'horizontal',
		nextText: '',
		prevText: '',
		useCSS: true,
		speed: 500,
		infiniteLoop: true,
		hideControlOnEnd: true,
		easing: 'easeOutElastic',
	});

	$('.bxslider2').bxSlider({
		pager: false,
		minSlides: 1,
		maxSlides: 4,
		moveSlides: 1,
		nextText: '',
		prevText: '',
		slideMargin: 30,
		slideWidth: 270
	});

	$('.bxslider3').bxSlider({
		pager: false,
		minSlides: 1,
		maxSlides: 6,
		moveSlides: 6,
		nextText: '',
		prevText: '',
		easing: 'jswing',
		slideWidth: 170,
		slideMargin: 30,
	});

	$('.toggle-menu').click(function () {
		$('.menu-container').toggleClass('menu-open');
		$(this).toggleClass('active');
	});
});